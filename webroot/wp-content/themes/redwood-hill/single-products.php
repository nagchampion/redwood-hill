<?php
/**
 * The template for displaying all single posts.
 *
 * @package redwood-hill
 */

//VARS 

	$post_id = $post->ID;

    $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        
    $title = get_the_title($post_id);
    $content = the_content();
    
    $size = get_field('exp_product_size', $post_id);
    $terms = get_the_terms( $post_id, 'dairy' );

    //coupon 
    $hasCoupon = get_field('exp_product_has_coupon');
    $couponTxtField = get_field('exp_product_coupon_text');

    if ($couponTxtField):
    	$couponTxt = $couponTxtField;
    else: 
    	$couponTxt = "View Coupon";
    endif;

    $couponURL = get_field('exp_product_coupon_url');

    $ingredients = get_field('exp_product_ingredients', $post_id);
    $cultures = get_field('exp_product_live_cultures', $post_id);

    $certifcations = get_field('exp_product_certifications');
    $destini = get_field('exp_product_destini_id');

    $thisTerm = $terms[0]->name;
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			
				
			<div class="product-single__top">
				<div class="content">
					<div class="product__image <?php echo strtolower($terms[0]->name); ?>">
						<img src="<?php echo $URL; ?>" />
					</div><!-- image -->
					<div class="block__txt">
						<div class="categories"><?php echo $terms[0]->name; ?></div>
						<div class="product-title"><?php echo $title; ?></div>
						<div class="size"><?php echo $size; ?></div>
						<div class="product-content"><?php echo wpautop(get_the_content()); ?></div>

						<?php if ( (!$hasCoupon) && $thisTerm == 'Cheese'): ?>
						
						<?php else: ?>
						<div class="coupons">
							<ul>
								<li id="find-product" class="<?php echo strtolower($terms[0]->name); ?>" data-dest-id="<?php echo $destini; ?>">
									<a href="<?php echo site_url(); ?>/store-finder">
										<svg xmlns="http://www.w3.org/2000/svg" width="18.75" height="25" viewBox="0 0 18.75 25"><title>Untitled-3</title><path d="M9.37,0A9.37,9.37,0,0,0,0,9.37C0,14.55,9.37,25,9.37,25s9.37-10.45,9.37-15.63A9.37,9.37,0,0,0,9.37,0Zm0,12.19a3,3,0,1,1,3-3A3,3,0,0,1,9.37,12.19Z" fill="#676767"/></svg>
										<span>Find This Product Nearby</span>
									</a>
								</li>
								
								<?php if($hasCoupon): ?>
								<li>
									<a href="<?php echo $couponURL; ?>" target="_blank">
										<svg xmlns="http://www.w3.org/2000/svg" width="36.23" height="20.7" viewBox="0 0 36.23 20.7"><title>coupon</title><path d="M25.88,0H0V20.7H25.88L36.23,10.35Zm-.51,13.66a3.31,3.31,0,1,1,3.31-3.31A3.31,3.31,0,0,1,25.37,13.66Z" fill="#676767"/></svg>
										<span><?php echo $couponTxt; ?></span>
									</a>
								</li>
								<?php endif; //hasCoupon ?>
							</ul>
						</div><!-- coupons -->
					<?php endif; ?>
						<div class="certification">
							<h6>Certifications</h6>
							<?php
					        
						    if( $certifcations ): ?>
						        
						        <?php foreach( $certifcations as $c ): 
						            
						            $post_id = $c->ID;

						            $URL = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
						      
						            ?>
						            <div class="certifcation-image">
						              <img src="<?php echo $URL; ?>" />
						            </div><!-- cert image -->
						        <?php endforeach; ?>
						        
						    <?php endif; ?>
						</div>
					</div>
				</div>
			</div><!-- product top -->

			<div class="product-single__bottom <?php echo strtolower($terms[0]->name); ?>">
				<div class="product__ingredients">
					<h4>Ingredients</h4>
					<div class="ingredients">
						<?php echo $ingredients; ?>
					</div>  
					<h4 class="ingredients-header">Live Active Cultures</h4>
					<div class="ingredients cultures">
						<?php echo $cultures; ?>
					</div>
				</div><!-- ingredients -->
				<div class="product__nutrition">
					<h4>Nutrition Facts</h4>
					<div class="column">
						<div class="serving-size">
							<?php 
    							while ( have_rows('exp_product_serving_size') ) : the_row(); 
    						?>

    							<div class="left"><?php the_sub_field('exp_serving_size_text'); ?></div>
    							<div class="right"><?php the_sub_field('exp_serving_size_value'); ?></div>

    						<?php
							    endwhile;
							?>
						</div><!-- serving size -->
						<div class="calories">
							
							<?php 
    							while ( have_rows('exp_product_calories') ) : the_row(); 
    						?>

    							<div class="left"><?php the_sub_field('exp_calories_text'); ?></div>
    							<div class="right"><?php the_sub_field('exp_calories_value'); ?></div>

    						<?php
							    endwhile;
							?>
						</div><!-- calories -->
						<div class="main-nutrition">
							<h6>% Daily Value*</h6>
							<?php 
    							while ( have_rows('exp_product_nutrition_main') ) : the_row(); 
    						?>

    							<div class="left<?php if ( get_sub_field('exp_nutrition_main_indent')): echo ' indent'; endif; ?>"><?php the_sub_field('exp_nutrition_main_text'); ?></div>
    							<div class="right"><?php the_sub_field('exp_nutrition_main_value'); ?></div>

    						<?php
							    endwhile;
							?>
						</div><!-- main nutrition -->

					</div>
					<div class="column">
						<div class="vitamins">
							<!--<h6>% Daily Value*</h6>-->

							<?php 
    							while ( have_rows('exp_product_nutrition_vitamin') ) : the_row(); 
    						?>

    							<div class="left"><?php the_sub_field('exp_nutrition_vitamin_text'); ?></div>
    							<div class="right"><?php the_sub_field('exp_nutrition_vitamin_value'); ?></div>

    						<?php
							    endwhile;
							?>
							
						</div><!-- vitamins -->
						<p class="percent-daily">
								* The Percent Daily Values are based on a 2,000 calorie diet.
							</p>
					</div>
				</div><!-- nutrition -->

			</div><!-- product bottom -->

				<?php endwhile; // end of the loop. 
				wp_reset_query();
				?>
			<section class="may-also-like">
				<div class="content">
					<h5>You May Also Like...</h5>
				<?php 
					
					$selectPosts = get_field('exp_product_may_also_like');
        
				    if( $selectPosts ): ?>
				        
				        <?php foreach( $selectPosts as $post ): 
			          
			          
			          	global $post;
			          	$post_id = $post->ID;

			            $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			                
			            $title = get_the_title($post_id);
			            
			            $link = get_permalink($post_id);
			            $size = get_field('exp_product_size', $post_id);
			            $queryTerm = get_the_terms($post_id, 'dairy');
			            $term = $queryTerm[0]->name;


			            ?>

			            <div class="product-block<?php echo ' ' . strtolower($term); ?>">
							<a href="<?php echo $link; ?>" >
								<div class="product__image">
									<img src="<?php echo $URL; ?>" />
								</div>
							</a>
							<div class="block__txt">
								<div class="categories"><?php echo $term; ?></div>
								<div class="product-title"><?php echo $title; ?></div>
								<div class="size"><?php echo $size; ?></div>
							</div>
						</div><!-- blog-block --> 


			            <?


			            
		             	endforeach;
		                endif; 
			          // Reset Query
			          wp_reset_query();
			          
			          

			          


			          ?>
			          
				</div><!-- content -->
			</section><!-- may also like -->
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>