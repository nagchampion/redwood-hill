<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package redwood-hill
 */

$header = get_field('exp_page_header', 'option');
$subheader = get_field('exp_page_subheader', 'option');
$hero = get_field('exp_hero_image', 'option');
$heroURL = $hero['url'];

$shortHero = get_field('exp_short_hero', 'option');
get_header(); ?>


	<div id="primary" class="content-area">
        

        
           
            <section class="hero<?php if($shortHero) echo' short'; ?>">
             
               <div class="content">
                 <div class="in" style="background-image: url('<?php echo $heroURL; ?>');"></div>
                 <div class="lockup">
                   <h1><?php echo $header; ?></h1>
                   <h2><?php echo $subheader; ?></h2>
                 </div>
               </div>
             
            </section><!-- hero -->
            <section class="intro-copy">
              <div class="intro-copy__text">
                <?php echo the_field('exp_ra_intro_text', 'options'); ?>
              </div><!-- txt -->
            </section><!-- intro copy -->
            
            <section class="recipe-categories">
              
                <div class="filters filters-recipe">
                  <!--<div class="lbl"><?php the_field('sort_by','option');?> <img class="arrow" src="<?php echo get_template_directory_uri(); ?>/img/arrows/arrow-down.svg" /></div>-->
                  <span class="filter-lead">Sort By:</span> 
                  <ul>
                    <li class="filter-item selected"><a data-type="team-category" data-filter="all" href="<?php echo site_url(); ?>/recipes">All</a></li>
                    <?php 
                     $terms = get_terms(
                      array(
                          'taxonomy' => 'recipe',
                          'hide_empty' => false
                      )
                     );
                     if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                         foreach ( $terms as $term ) {

                          $class = ( is_tax( 'recipe', $term->slug ) ) ? ' selected' : '';
                          $link = get_term_link( $term );
                   
                        echo '<li class="filter-item faq-filter' . $class . '"><a href="'. get_category_link( $term->term_id ) . '" data-filter="'.$term->slug.'">' . $term->name . '</a></li>';     
                         }
                                    
                     }
                     ?>
                   </ul>    
                </div>
              
            </section><!-- recipe categories -->
            <section class="the-recipes">
              <div class="the-recipes-wrapper">
              <?php 

                      $recQuery = array(
                        'posts_per_page' => -1,
                        'orderby'        => 'menu_order',
                        'post_type'      => 'recipes'
                    );

                  query_posts( $recQuery ); ?>

                  
                  
                  <?php if(get_posts($recQuery)) : 

                      // The Loop
                      while ( have_posts() ) : the_post();
                      
                      
                        global $post;
                        $post_id = $post->ID;

                        $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            
                        $title = get_the_title($post_id);
                        $content = get_the_content($post_id);
                        $link = get_permalink($post_id);
                        $terms = get_the_terms($post_id, 'recipe');
                        

                         include(locate_template('inc/recipe-block.php')); ?>
                        
                      
                      <?php endwhile;
                            endif; 
                      // Reset Query
                      wp_reset_query();
              ?>
              </div><!-- the recipes wrapper -->
            </section><!-- receipe block -->
            <?php sk_the_option_page_blocks(); ?>
       

    </div><!-- #primary -->

<?php get_footer(); ?>
