<?php
/**
 * The template for displaying all single posts.
 *
 * @package redwood-hill
 */

//VARS 

	$post_id = $post->ID;

    $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        
    $title = get_the_title($post_id);
    $content = get_the_content($post_id);
    
   $header = get_field('exp_page_header');
	$subheader = get_field('exp_page_subheader');
	

	$shortHero = get_field('exp_short_hero');

	

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<section class="no-print hero <?php if($shortHero) echo' short'; ?> no-bg">
             
               <div class="content">
                 <div class="in" style="background-image: url('<?php echo $URL; ?>');"></div>
                 <div class="lockup">
                   <h1><?php echo $title; ?></h1>
                   
                 </div>
               </div>
             
            </section><!-- hero -->
			
			<section class="recipe-single__content">
				<?php echo get_the_content($post_id); ?>
				
			</section>
			
		<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>