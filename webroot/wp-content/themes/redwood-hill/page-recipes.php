<?php
/**
 * 
 * Template Name: Recipes Landing
 *
 * @package redwood-hill
 */
$header = get_field('exp_page_header');
$subheader = get_field('exp_page_subheader');
$hero = get_field('exp_hero_image');
$heroURL = $hero['url']; 

$shortHero = get_field('exp_short_hero');

// Mobile Image Override 
$mobileImage = get_field('exp_mobile_image_override');
$heroMobile = get_field('exp_mobile_hero_image');
$heroURLMobile = $heroMobile['url'];

// Slider Image Override 
$heroSlider = get_field('exp_hero_slider');
$heroSliderImages = get_field('exp_hero_slider_images');
get_header(); ?>


	<div id="primary" class="content-area">
        

        <?php while ( have_posts() ) : the_post(); ?>
           
            <?php if($heroSlider): ?>
            <section class="hero slider<?php if($shortHero) echo ' short'; ?><?php if(!$hero) echo ' no-bg'; ?><?php if($mobileImage) echo ' mobileImage'; ?>">
             
               <div class="sk-slider">
                 <?php if( have_rows('exp_hero_slider_images') ):

                  // loop through the rows of data
                  while ( have_rows('exp_hero_slider_images') ) : the_row();

                      // display a sub field value
                      $slideImage = get_sub_field('image');
                      $slideImageURL = $slideImage['url'];
                  ?>
                  <div class="content sk-slide">
                    <div class="in" style="background-image: url('<?php echo $slideImageURL; ?>');"></div>
                  </div>
                  <?php
                  endwhile;
                endif;
                ?>
      
              </div> <!-- slider -->
              <?php if($mobileImage): ?>
                  <div class="content mobile-content">
                    <div class="mobile-in" style="background-image: url('<?php echo $heroURLMobile; ?>');"></div>
                  </div>
              <?php endif; ?>
               
               <div class="lockup">
                   <h1><?php echo $header; ?></h1>
                   <h2><?php echo $subheader; ?></h2>
                 </div>
            </section><!-- hero image slider -->
            <?php else: ?>
            <section class="hero<?php if($shortHero) echo ' short'; ?><?php if(!$hero) echo ' no-bg'; ?><?php if($mobileImage) echo ' mobileImage'; ?>">
             
               <div class="content">
                 <div class="in" style="background-image: url('<?php echo $heroURL; ?>');"></div>
                 <?php if($mobileImage): ?>
                    <div class="mobile-in" style="background-image: url('<?php echo $heroURLMobile; ?>');"></div>
                 <?php endif; ?>
                 <div class="lockup">
                   <h1><?php echo $header; ?></h1>
                   <h2><?php echo $subheader; ?></h2>
                 </div>
               </div>
            
            </section><!-- hero static -->
            <?php endif; ?>
            <section class="intro-copy">
              <div class="intro-copy__text">
                <?php echo the_content(); ?>
              </div><!-- txt -->
            </section><!-- intro copy -->
            
            <section class="recipe-categories">
              
                <div class="filters filters-recipe">
                  <!--<div class="lbl"><?php the_field('sort_by','option');?> <img class="arrow" src="<?php echo get_template_directory_uri(); ?>/img/arrows/arrow-down.svg" /></div>-->
                  <span class="filter-lead">Sort By:</span> 
                  <ul>
                    <li class="filter-item filtro selected"><a data-type="team-category" data-filter="all">All</a></li>
                    <?php 
                     $terms = get_terms(
                      array(
                          'taxonomy' => 'recipe',
                          'hide_empty' => false
                      )
                     );
                     if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                         foreach ( $terms as $term ) {
                          $link = get_term_link( $term );
                   
                        echo '<li class="filter-item filtro"><a href="#" data-filter="'.$term->slug.'">' . $term->name . '</a></li>';     
                         }
                                    
                     }
                     ?>
                   </ul>    
                </div>
              
            </section><!-- recipe categories -->
            <section class="the-recipes">
              <div class="the-recipes-wrapper">
              <?php 

                      $recQuery = array(
                        'posts_per_page' => -1,
                        'orderby'        => 'menu_order',
                        'post_type'      => 'recipes'
                    );

                  query_posts( $recQuery ); ?>

                  
                  
                  <?php if(get_posts($recQuery)) : 

                      // The Loop
                      while ( have_posts() ) : the_post();
                      
                      
                        global $post;
                        $post_id = $post->ID;

                        $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            
                        $title = get_the_title($post_id);
                        $content = get_the_content($post_id);
                        $link = get_permalink($post_id);
                        $terms = get_the_terms($post_id, 'recipe');
                        

                         include(locate_template('inc/recipe-block.php')); ?>
                        
                      
                      <?php endwhile;
                            endif; 
                      // Reset Query
                      wp_reset_query();
              ?>
              </div><!-- the recipes wrapper -->
            </section><!-- receipe block -->
            <?php sk_the_page_blocks(); ?>
        <?php endwhile; // end of the loop. ?>

    </div><!-- #primary -->

<?php get_footer(); ?>
