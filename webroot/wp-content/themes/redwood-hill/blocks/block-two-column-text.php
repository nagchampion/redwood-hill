<?php 
//
// BLOCK - LG Banner
//
// Adds a simple copy block
//


$contentLeft = get_sub_field('exp_block_content');
$contentRight = get_sub_field('exp_block_content--two');


?>


	
	<div class="content">
		<div class="two-column-block text">
			<?php echo $contentLeft; ?>
		</div><!-- two column block -->
		<div class="two-column-block text">
			<?php echo $contentRight; ?>
		</div><!-- two column block -->
		
		
	</div><!-- content -->
		
		

