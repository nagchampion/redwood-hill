<?php 
//
// BLOCK - LG Banner
//
// Adds a simple copy block
//

$image = get_sub_field('exp_block_image');
$imageURL = $image["url"];
$content = get_sub_field('exp_block_content');
$cta = get_sub_field('exp_block_cta');
$rightLeft = get_sub_field('exp_right_left');

?>


   	<a class="cta slideOut" href="#"><?php echo $cta; ?></a>
	<div class="slideOut-wrapper">

		<div class="content imageRight">
			<div class="two-column-block image">
				<div class="in" style="background-image:url('<?php echo $imageURL; ?>')"></div>
			</div><!-- two column block -->
			<div class="two-column-block text">
				<div class="contact-info-block">
					<?php echo $content; ?>
				</div><!-- contact info block -->
			</div><!-- two column block -->
		</div><!-- content -->

	</div><!-- slideout wrapper -->
		
		

