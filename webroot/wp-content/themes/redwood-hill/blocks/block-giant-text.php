<?php 
//
// BLOCK - Copy Block
//
// Adds a simple copy block
//


$content = get_sub_field('exp_block_content');
$twoCol  = get_sub_field('exp_two_column');

?>


	
	<div class="content<?php if($twoCol): echo ' two-column'; endif; ?>">
			<?php echo $content; ?>
	</div><!-- content -->
		
		

