<?php 
//
// BLOCK - LG Banner
//
// Adds a simple copy block
//

$image = get_sub_field('exp_block_image');
$content = get_sub_field('exp_block_content');
$cta = get_sub_field('exp_block_cta');
$url = get_sub_field('exp_block_cta_url');

	
?>
		

			<div class="banner-image">
                        
          		<div class="in" style="background-image: url('<?php echo $image["url"]; ?>' );"></div>
	        	<div class="lockup right">
                   <div class="banner-text"><?php echo $content; ?></div>
                   <?php if($cta) : ?>
                   		<a class="cta" href="<?php echo $url; ?>"><?php echo $cta; ?></a>
                   <?php endif; ?>
                 </div>
	        </div><!-- banner image -->

	
		

