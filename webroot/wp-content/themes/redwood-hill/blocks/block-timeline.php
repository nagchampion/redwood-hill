<?php 
//
// BLOCK - LG Banner
//
// Adds a simple copy block
//


?>

	<div class="content">
	<div id="timeline" class="sk-slider">
		
    <?php 
              $timelineCategory = get_sub_field('exp_timeline_category');
              $timelineCategorySlug = $timelineCategory->slug;
              

              $timelineQuery = array(
                'posts_per_page' => -1,
                'orderby'        => 'menu_order',
                'post_type'      => 'history-stories',
                'tax_query' => array(
                    array (
                        'taxonomy' => 'history-story-type',
                        'field' => 'slug',
                        'terms' => $timelineCategorySlug,
                    )
                ),
            );

          query_posts( $timelineQuery ); ?>

          
          
          <?php if(get_posts($timelineQuery)) : 

              // The Loop
              while ( have_posts() ) : the_post();
              
                
                global $post;
                
                $post_id = $post->ID;

                $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                    
                $title = get_the_title($post_id);
                $content = get_the_content($post_id);?>
                

                 <div class="imageRight sk-slide">
					<div class="two-column-block image">
						<div class="in" style="background-image:url('<?php echo $URL; ?>')"></div>
					</div><!-- two column block -->
					<div class="two-column-block text">
						<div class="contact-info-block">
							<h3><?php echo $title; ?></h3>
							<?php echo $content; ?>
							
						</div><!-- contact info block -->
					</div><!-- two column block -->
				</div><!-- content -->
                
              
              <?php endwhile;
                    endif; 
              // Reset Query
              wp_reset_query();
      		?>
	
		


		
	</div><!-- timeline -->
	</div>
