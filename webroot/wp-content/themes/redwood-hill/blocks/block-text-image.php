<?php 
//
// BLOCK - LG Banner
//
// Adds a simple copy block
//

$image = get_sub_field('exp_block_image');
$imageURL = $image["url"];
$content = get_sub_field('exp_block_content');
$cta = get_sub_field('exp_block_cta');
$url = get_sub_field('exp_block_cta_url');
$rightLeft = get_sub_field('exp_right_left');
//$borderBot = get_sub_field('exp_border_bottom');

?>


	
	<div class="content<?php if($rightLeft): echo ' imageRight'; endif; ?>">
		<div class="two-column-block image">
			<div class="in" style="background-image:url('<?php echo $imageURL; ?>')"></div>
		</div><!-- two column block -->
		<div class="two-column-block text">
			<div class="contact-info-block">
				<?php echo $content; ?>
				<?php if($cta) : ?>
               		<a class="cta" href="<?php echo $url; ?>"><?php echo $cta; ?></a>
               <?php endif; ?>
			</div><!-- contact info block -->
		</div><!-- two column block -->
		
		
	</div><!-- content -->
	<div style="clear:both"></div>	
		

