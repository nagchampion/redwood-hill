<?php 
//
// BLOCK - LG Banner
//
// Adds a simple copy block
//



$terms = get_terms('dairy');


?>
	<div class="content">
	<?php foreach ($terms as $term) {
	 $queryTerm = strtolower($term->name);
	 $queryDesc = $term->description;
	 
        $prodQuery = array(
            'posts_per_page' => -1,
            'orderby'        => 'menu_order',
            'post_type'      => 'products',
            'tax_query' => array(
		        array (
		            'taxonomy' => 'dairy',
		            'field' => 'slug',
		            'terms' => $queryTerm,
		        )
		      ),
        );

	    query_posts( $prodQuery ); ?>

	    <div class="product-wrapper" id="<?php echo $queryTerm; ?>">
	    <h2><?php echo $queryTerm; ?></h2>
	    <div class="product-desc"><?php echo $queryDesc; ?></div>
	    <? if(get_posts($prodQuery)) : 

          // The Loop
          while ( have_posts() ) : the_post();
          
          
          	global $post;
          	$post_id = $post->ID;

            $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                
            $title = get_the_title($post_id);
            $content = get_the_content($post_id);
            $link = get_permalink($post_id);
            $size = get_field('exp_product_size', $post_id);
            

            ?>


            <?php include(locate_template('inc/product-block.php')); ?>
            
          
          <?php endwhile;
                endif; 
          // Reset Query
          wp_reset_query();
          ?>

         </div><!-- product wrapper -->

	<? } // end terms foreach ?>
	
		
	</div><!-- content -->
		
		

