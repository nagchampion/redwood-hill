<?php 
//
// BLOCK - Blog 3 up
//
// Adds a simple copy block
//






?>

	
	<div class="content">
		<?php 
		
        

        $selectPosts = get_sub_field('exp_select_a_post');
        
    if( $selectPosts ): ?>
        
        <?php foreach( $selectPosts as $p ): 
            
            $post_id = $p->ID;

            $URL = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
                
            $title = get_the_title($post_id);
            $content = $p->post_content;
            $link = get_permalink($post_id);
      
            ?>
            <div class="blog-block post-block">
              <a href="<?php echo $link; ?>" >
                <div class="block__image">
                  <div class="in" style="background-image: url('<?php echo $URL; ?>');"></div>
                </div>
              </a>
              <a href="<?php echo $link; ?>" >
                <div class="block__txt">
                  
                  <h3><?php echo $title; ?></h3>
                  <div class="txt"><?php echo wp_trim_words( $content, 20, '...' ); ?></div>
                </div>
              <a href="<?php echo $link; ?>" >
            </div><!-- blog-block -->
        <?php endforeach; ?>
        
    <?php endif; ?>
      

          
		
	</div><!-- /content -->


