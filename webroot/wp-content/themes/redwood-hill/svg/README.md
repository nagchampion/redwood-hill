# SVG

Place all SVG's in here for use as icons by way of inline SVG. Fallbacks can be placed using the same naming structure in the `img/` directory. Optimized svgs will be placed inside a `build` directory.