# The Arsenal

This directory is where all javascript modules live. All available modules exist inside the `_arsenal` in the project root. Set up the modules you'll need in this project in the `setup.json` file (also in the project root), and then run `gulp build-arsenal` to copy the selected scripts in to this directory.