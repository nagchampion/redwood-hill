<?php

/**
 * This class is an example implementation of a PHP based landing page integration.
 * Note: This is a template, which is used at your own risk.
 */
class LandingPageIntegration
{

    /**
     * The BasePath is the URL for the system including the landing page result OData service.
     *
     * @var string
     */
    const BASE_PATH = "https://my300810-api.s4hana.ondemand.com/sap/opu/odata/sap/CUAN_CONTENT_PAGE_RESULT_SRV/";

    /**
     * The ResultHeadersPath is the name of the ResultHeaders entity which is used for processing the landing page results.
     *
     * @var string
     */
    const RESULT_HEADERS_PATH = "ResultHeaders";

    /**
     * The credentials are used for authenticating on the system.
     * This is usually a dedicated system or communication user with the integration role assigned.
     *
     * @var string
     */
    const CREDENTIALS = 'Landingpage_user:eJyRsfLMJ2Vznd}SDitJEmFXBFfpbtRThUZwCLja';

    /**
     * The cookies are remembered between consecutive OData requests to implement the session handling and security measures of the SAP Gateway.
     *
     * @var string
     */
    private $cookies = "";

    /**
     * The X-CSRF-Token is required for the OData service communication and must be fetched before it is possible to perform any changing requests such as 'POST'.
     *
     * @var string
     */
    private $xCsrfToken = null;

    /**
     * This method is the main entry point for processing the requests received landing page.
     */
    public function execute()
    {
        session_start();

        switch ($_SERVER["REQUEST_METHOD"]) {
            case "POST":
                $this->handlePostRequest();
                break;
        }
    }

    /**
     * POST requests must be forwarded to the system and the responses must be passed to the client to ensure correct landing page integration.
     */
    private function handlePostRequest()
    {
        // first fetch the x-csrf-token
        $this->fetchXCsrfToken();
        
        // read the POST data sent by the landing page
        $requestBody = @file_get_contents("php://input");
        $requestData = json_decode($requestBody);
        
        // enhance the request data with session id
        $requestData->SessionId = session_id();
        
        // enhance the request data with (optional) IP address & campaign id
        $requestData->IpAddress = $_SERVER["REMOTE_ADDR"];
        $requestData->CampaignId = "142"; 
        
        // add the concat information for identification
        $requestData->ContactFacets = $this->getContactFacets();
        
        // send the prepared request data to the system
        $requestString = json_encode($requestData);
        $response = $this->sendHttpRequest("POST", $this::BASE_PATH . $this::RESULT_HEADERS_PATH, $requestString);
        
        // print the response
        echo $response;
    }

    /**
     * The landing page OData service supports contact identification using ContactIdOrigin and ContactId value pairs.
     * This example reads the corresponding values of the session data.
     *
     * @return stdClass[]
     */
    private function getContactFacets()
    {
        $contactIdOrigin = null;
        $contactId = null;
        if (isset($_SESSION["CONTACT_ID_ORIGIN"])) {
            $contactIdOrigin = $_SESSION["CONTACT_ID_ORIGIN"];
        }
        if (isset($_SESSION["CONTACT_ID"])) {
            $contactId = $_SESSION["CONTACT_ID"];
        }

        $contactFacets = array();

        if ($contactIdOrigin && $contactId) {
            $contactFacet = new stdClass();
            $contactFacet->IdOrigin = $contactIdOrigin;
            $contactFacet->Id = $contactId;
            array_push($contactFacets, $contactFacet);
        }

        return $contactFacets;
    }

    /**
     * Send a 'HEAD' request to fetch the required X-CSRF-Token from the OData service.
     * If the HEAD request fails, a 'GET' request is performed.
     */
    private function fetchXCsrfToken()
    {
        $this->sendHttpRequest("HEAD", $this::BASE_PATH, null);
        if (!$this->xCsrfToken) {
            // HEAD request failed -> fallback using GET
            $this->sendHttpRequest("GET", $this::BASE_PATH, null);
        }
    }

    /**
     * This method performs a synchronous HTTP request and returns its response.
     *
     * @param string $method
     *            The HTTP method (e.g. 'HEAD', 'POST')
     * @param string $path
     *            The URL for the request
     * @param string $body
     *            The request payload (POST data)
     * @return string The response
     */
    private function sendHttpRequest($method, $path, $body)
    {
        // first create stream context
        $context = $this->createStreamContext($method, $body);
        
        // perform http request
        $response = file_get_contents($path . "?saml2=disabled", false, $context);

        if ($response === false) {
            // request failed - print error for analysis
            echo error_get_last();
        }
        
        // process response headers
        $this->readResponseHeaders($http_response_header);
        
        // return response
        return $response;
    }

    /**
     * This method creates a stream context, which is used for the HTTP request.
     * It configures the context for the authorization, content-type, cookies, and x-csrf-token.
     *
     * @param string $method
     *            The HTTP method
     * @param string $body
     *            The request payload (POST data)
     * @return resource The stream context
     */
    private function createStreamContext($method, $body)
    {
        // basic authorization uses base64 encoded credentials
        $credentials = base64_encode($this::CREDENTIALS);
        
        // build http request headers
        $headers = array(
            "Authorization: Basic " . $credentials,
            "Accept: application/json",
            "Content-Type: application/json"
        );

        if ($this->cookies) {
            // add remembered cookies
            array_push($headers, "Cookie: " . $this->cookies);
        }
        
        // add x-csrf-token header for fetching or using the already fetched token
        $xCsrfToken = ($this->xCsrfToken ? : "Fetch");
        array_push($headers, "x-csrf-token: " . $xCsrfToken);
        
        // build complete options array
        $options = array(
            "http" => array(
                "header" => $headers,
                "method" => $method,
                "content" => $body,
                "ignore_errors" => true,
                "max_redirects" => 0
            ),
            'ssl' => array(
                'verify_peer' => false,
            )
        );
        
        // return stream context using the built options
        return stream_context_create($options);
    }

    /**
     * This method processes the HTTP response headers in order to read the fetched X-CSRF-Token and cookies
     *
     * @param array $responseHeaders            
     */
    private function readResponseHeaders($responseHeaders)
    {
        // loop response headers
        foreach ($responseHeaders as $responseHeader) {
            // split header name from value
            $parts = explode(" ", $responseHeader);
            
            // handle response header based on name
            switch ($parts[0]) {
                case "x-csrf-token:":
                    // save fetched x-csrf-token
                    $this->xCsrfToken = $parts[1];
                    break;
                case "set-cookie:":
                    // set received cookies
                    $this->cookies .= $parts[1];
                    break;
            }
        }
    }
}

// initialize the integration class and start the processing
$landingPageIntegration = new LandingPageIntegration();
$landingPageIntegration->execute();
