
//
// namespace - SK
//

(function(SK, $, undefined){

    var $window;


    /* --------------------------------------------
     * --util
     * -------------------------------------------- */


    /**
     * Creates and returns a new, throttled version of the passed
     *  function, that, when invoked repeatedly, will only actually
     *  call the original function at most once per every 'threshold'
     *  milliseconds. Useful for rate-limiting events that occur
     *  faster than you can keep up with.
     * @link - https://remysharp.com/2010/07/21/throttling-function-calls
     */
    function throttle(fn, threshhold, scope) {
      threshhold || (threshhold = 250);
      var last,
          deferTimer;
      return function () {
        var context = scope || this;

        var now = +new Date,
            args = arguments;
        if (last && now < last + threshhold) {
          // hold on to it
          clearTimeout(deferTimer);
          deferTimer = setTimeout(function () {
            last = now;
            fn.apply(context, args);
          }, threshhold);
        } else {
          last = now;
          fn.apply(context, args);
        }
      };
    }

    /* --------------------------------------------
     * --func
     * -------------------------------------------- */
     $('#nav-toggle').on('click', function(){
      if ( $(this).hasClass('is-active') ) {
        $(this).removeClass('is-active');
        $('html').removeClass('nav-open');
        $('#site-navigation').slideToggle('fast');

      } else {
        $(this).addClass('is-active');
        $('html').addClass('nav-open');
        $('#site-navigation').slideToggle('fast');
      }
    });
     
     function slideOutToggle() {
      $('a.cta.slideOut').on('click', function(e) {
        e.preventDefault();
        $(this).next('.slideOut-wrapper').slideToggle(); 
      });
     }

     var search = document.getElementById('searchToggle');

     search.addEventListener('click', function(){
      $('.search-box form').slideToggle();
      $(".search-field").focus();
     });

     // BLOG FILTER
    // $('.filtro a').click(function(e){
    //   e.preventDefault();
    //   var fValue = $(this).data('filter');

    //   $(this).closest('.filtro').siblings().each(function(){
    //     $(this).removeClass('selected');
    //   });
    //   $(this).closest('.filter-item').addClass('selected');
    //   filterItems(fValue);
      
    // });

    function mobileNav() {
      var subnav   = $('div.sub-nav'),
          _ul      = $('#calendarTimes');
          
      subnav.on('click', function() {
        var is_small = $(window).width() <  750 ? true : false;
        if(is_small) {

              _ul.slideToggle('fast');  

          }   
        
      });
      
    }

    //
    // ajaxchimp
    //
    function mailKimp() {
      $('.footer-mailchimp').ajaxChimp({
          callback: function(resp){
              var msg = resp.msg;
      
              if(resp.result === 'success'){

                  
                  $('.footer-mailchimp input[type="email"]').val('');
                  $('.label-container').text('Thank you!');
              }
              /*if(resp.result === 'error'){
                  var sub = "0 -";
                  if ( msg.indexOf(sub) !== -1 ) {
                      txtMsg = msg.split('-');
                      msg = txtMsg[1];
                  } 
                  modals.create('<header class="modal-header"><h4>Our Apologies</h4></header><div class="modal-main">' + msg + '</div>');
              }*/
          }
      });
    }
    function productFinder() {
      const modal = document.getElementById('find-product');
      const close = document.getElementById('close');
      const prodB = document.getElementById('productZip');

      modal.onclick = function(){
        $('html').toggleClass('show-modal');
      }
      close.onclick = function() {
        $('html').toggleClass('show-modal');
      }

      prodB.onclick = function() {
        let zip = document.getElementById("zipCode").value;

        if (zip && zip.length == 5) {
          let destCode = modal.getAttribute('data-dest-id');
          alert(destCode);
          alert(zip);
          location.href = 'http://redwood-hill.thexperiment.com/webroot/store-finder/?zip=' + zip + '&product=' + destCode;
        }
        
      }

    }
    function filterItems(val){
      $('.the-recipes-wrapper .recipe-block, .the-faqs-wrapper .faq-block').each(function(){
        var t = $(this);
        var tLoc = t.data('categories');
        
        t.fadeOut(200);
        t.removeClass('active-show show-all');
        t.css( "clear", "" );

        setTimeout(function(){
          if (tLoc.includes(val)) {
            t.fadeIn(200);
            t.addClass('active-show');
            if (val == 'all') {
              t.addClass('show-all');
              t.removeClass('active-show');
            }
          }
        }, 200);


      });
      var count = 1;
      setTimeout(function() {
         $('.the-recipes-wrapper .recipe-block.active-show').each(function () {
          if(count %4 == 0) {
            $(this).css('clear','both');
          }
          count++;
        }); 
      }, 220);

      setTimeout(function() {
         $('.the-blog-wrapper .post-block.active-show').each(function () {
          if(count %2 == 1) {
            $(this).css('clear','both');
          }
          count++;
        }); 
      }, 220);

    }
    function faqToggle() {
      $('.faq-block .title-wrapper').on('click', function(){
        var block = $(this).closest('.faq-block');

        if (block.hasClass('active')) {
          block.removeClass('active');
          block.find('.single-faq-content-wrapper').slideToggle();
        } else {
          block.addClass('active');
          block.find('.single-faq-content-wrapper').slideToggle();
        }
      });
    }
    function wrap(el, wrapper) {
      el.parentNode.insertBefore(wrapper, el);
      wrapper.appendChild(el);
    }

  // example: wrapping an anchor with class "wrap_me" into a new div element
  
    function imgCaption() {
      var images = document.querySelectorAll('img');

      images.forEach(function(el){
        var caption = el.getAttribute('title');
        var _this = el;

        if(caption) {
          wrap(_this, document.createElement('figure'));
          _this.parentNode.insertAdjacentHTML('beforeend', '<figcaption class="image-caption">' + caption + '</figcaption>');
        }
      });
      return;
    }
    /*function setBlogTitle() {
      var _selected = $('.filter-item.selected').text(),
          _day      = $('.subnav-filter.active'),
          _dayTitle = $('#calendarDay'),
          _title    = $('#categoryTitle');

          _title.text(_selected);
          _dayTitle.html(function() {

            var _justDay = _day.html();

            $('.subnav ul').slideToggle();
            return _justDay;

          });
    }*/

    /* --------------------------------------------
     * --public
     * -------------------------------------------- */

    //
    // init the things
    //
    SK.init = function(){

        // initialize some globals
        $window = $(window);
        slideOutToggle();
        mailKimp();
        faqToggle();
        //imgCaption();
        //productFinder();
    }


    // -------------------------------
    // DOM ready
    //
    $(document).ready(function(){
        SK.init();
    });

})(window.SK = window.SK || {}, jQuery);
