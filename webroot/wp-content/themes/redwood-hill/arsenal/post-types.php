<?php

/* --------------------------------------------
 *
 * Register post types
 *
 * -------------------------------------------- */
    
    //
    // @link http://codex.wordpress.org/Function_Reference/register_post_type
    //
    
    //
    // /**
    //  * register post type that has page for archive and single
    //  */
    // register_post_type('post_type', array(
    //     'labels'       => array(
    //         'name'               => __( 'post_type', 'redwood-hill' ),
    //         'singular_name'      => __( 'post_type', 'redwood-hill' ),
    //         'add_new_item'       => __( 'Add new post_type', 'redwood-hill' ),
    //         'edit_item'          => __( 'Edit post_type', 'redwood-hill' ),
    //         'new_item'           => __( 'New post_type', 'redwood-hill' ),
    //         'view_item'          => __( 'View post_type', 'redwood-hill' ),
    //         'search_items'       => __( 'Search post_types', 'redwood-hill' ),
    //         'not_found'          => __( 'No post_types found', 'redwood-hill' ),
    //         'not_found_in_trash' => __( 'No post_types found in Trash', 'redwood-hill' ),
    //     ),
    //     'supports' => array('title', 'editor', 'thumbnail'),
    //     'public'   => true,
    //     'rewrite'  => array(
    //         'slug' => 'post_type'
    //     )
    // ));
    //

    
    //
    // /**
    //  * register post type with NO single page
    //  */
    // register_post_type('post_type', array(
    //     'labels'       => array(
    //         'name'               => __( 'post_type', 'redwood-hill' ),
    //         'singular_name'      => __( 'post_type', 'redwood-hill' ),
    //         'add_new_item'       => __( 'Add new post_type', 'redwood-hill' ),
    //         'edit_item'          => __( 'Edit post_type', 'redwood-hill' ),
    //         'new_item'           => __( 'New post_type', 'redwood-hill' ),
    //         'view_item'          => __( 'View post_type', 'redwood-hill' ),
    //         'search_items'       => __( 'Search post_types', 'redwood-hill' ),
    //         'not_found'          => __( 'No post_types found', 'redwood-hill' ),
    //         'not_found_in_trash' => __( 'No post_types found in Trash', 'redwood-hill' ),
    //     ),
    //     'show_ui' => true,
    // ));
    //


add_action('init', 'sk_content_types');

/**
 * register content types
 */
function sk_content_types(){

    
    
    //
    // Products
    //
    register_post_type('products', array(
        'labels'       => array(
            'name'               => __( 'Products', 'redwood-hill' ),
            'singular_name'      => __( 'Product', 'redwood-hill' ),
            'add_new_item'       => __( 'Add new Product', 'redwood-hill' ),
            'edit_item'          => __( 'Edit Product', 'redwood-hill' ),
            'new_item'           => __( 'New Product', 'redwood-hill' ),
            'view_item'          => __( 'View Product', 'redwood-hill' ),
            'search_items'       => __( 'Search Products', 'redwood-hill' ),
            'not_found'          => __( 'No Products found', 'redwood-hill' ),
            'not_found_in_trash' => __( 'No Products found in Trash', 'redwood-hill' ),
        ),
        'description'  => __( "Products", 'redwood-hill' ),
        'supports'     => array('title', 'editor', 'thumbnail'),
        'hierarchical' => false,
        'public'       => true,
        'show_ui'      => true,
        'rewrite' => array( 'slug' => 'products', 'with_front' => false)
    ));


    
    
    //
    // Certifications
    //
    register_post_type('certifications', array(
        'labels'       => array(
            'name'               => __( 'Certifications', 'redwood-hill' ),
            'singular_name'      => __( 'Certification', 'redwood-hill' ),
            'add_new_item'       => __( 'Add new Certification', 'redwood-hill' ),
            'edit_item'          => __( 'Edit Certification', 'redwood-hill' ),
            'new_item'           => __( 'New Certification', 'redwood-hill' ),
            'view_item'          => __( 'View Certification', 'redwood-hill' ),
            'search_items'       => __( 'Search Certifications', 'redwood-hill' ),
            'not_found'          => __( 'No Certifications found', 'redwood-hill' ),
            'not_found_in_trash' => __( 'No Certifications found in Trash', 'redwood-hill' ),
        ),
        'description'  => __( "Available Certifications.", 'redwood-hill' ),
        'supports'     => array('title', 'thumbnail'),
        'public' => false,  
        'publicly_queryable' => true,  
        'show_ui' => true,  
        'hierarchical' => false
    ));

    //
    // Testimonials
    //
    register_post_type('testimonials', array(
        'labels'       => array(
            'name'               => __( 'Testimonials', 'redwood-hill' ),
            'singular_name'      => __( 'Testimonial', 'redwood-hill' ),
            'add_new_item'       => __( 'Add new Testimonial', 'redwood-hill' ),
            'edit_item'          => __( 'Edit Testimonial', 'redwood-hill' ),
            'new_item'           => __( 'New Testimonial', 'redwood-hill' ),
            'view_item'          => __( 'View Testimonial', 'redwood-hill' ),
            'search_items'       => __( 'Search Testimonial', 'redwood-hill' ),
            'not_found'          => __( 'No Testimonials found', 'redwood-hill' ),
            'not_found_in_trash' => __( 'No Testimonials found in Trash', 'redwood-hill' ),
        ),
        'description'  => __( "Available Testimonials.", 'redwood-hill' ),
        'supports'     => array('title', 'editor', 'thumbnail'),
        'public' => false,  
        'publicly_queryable' => true,  
        'show_ui' => true,  
        'hierarchical' => false
    ));

    //
    // History
    //
    register_post_type('history-stories', array(
        'labels'       => array(
            'name'               => __( 'History Items', 'redwood-hill' ),
            'singular_name'      => __( 'History Item', 'redwood-hill' ),
            'add_new_item'       => __( 'Add new History Item', 'redwood-hill' ),
            'edit_item'          => __( 'Edit History Item', 'redwood-hill' ),
            'new_item'           => __( 'New History Item', 'redwood-hill' ),
            'view_item'          => __( 'View History Item', 'redwood-hill' ),
            'search_items'       => __( 'Search History Item', 'redwood-hill' ),
            'not_found'          => __( 'No History Items found', 'redwood-hill' ),
            'not_found_in_trash' => __( 'No History Items found in Trash', 'redwood-hill' ),
        ),
        'description'  => __( "Available History Items.", 'redwood-hill' ),
        'supports'     => array('title', 'editor', 'thumbnail'),
        'public' => false,  
        'publicly_queryable' => true,  
        'show_ui' => true,  
        'hierarchical' => false
    ));

    //
    // FAQs
    //
    register_post_type('faqs', array(
        'labels'       => array(
            'name'               => __( 'FAQs', 'redwood-hill' ),
            'singular_name'      => __( 'FAQ', 'redwood-hill' ),
            'add_new_item'       => __( 'Add new FAQ', 'redwood-hill' ),
            'edit_item'          => __( 'Edit FAQ', 'redwood-hill' ),
            'new_item'           => __( 'New FAQ', 'redwood-hill' ),
            'view_item'          => __( 'View FAQ', 'redwood-hill' ),
            'search_items'       => __( 'Search FAQs', 'redwood-hill' ),
            'not_found'          => __( 'No FAQs found', 'redwood-hill' ),
            'not_found_in_trash' => __( 'No FAQs found in Trash', 'redwood-hill' ),
        ),
        'description'  => __( "Available FAQs.", 'redwood-hill' ),
        'supports'     => array('title', 'editor', 'thumbnail'),
        'public'       => true,
        'has_archive'  => 'faqs',
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'faqs/category', 'with_front' => false)
        //'menu_icon' => 'dashicons-admin-home'
    ));

    //
    // Recipes
    //
    register_post_type('recipes', array(
        'labels'       => array(
            'name'               => __( 'Recipes', 'redwood-hill' ),
            'singular_name'      => __( 'Recipe', 'redwood-hill' ),
            'add_new_item'       => __( 'Add new Recipe', 'redwood-hill' ),
            'edit_item'          => __( 'Edit Recipe', 'redwood-hill' ),
            'new_item'           => __( 'New Recipe', 'redwood-hill' ),
            'view_item'          => __( 'View Recipe', 'redwood-hill' ),
            'search_items'       => __( 'Search Recipes', 'redwood-hill' ),
            'not_found'          => __( 'No Recipes found', 'redwood-hill' ),
            'not_found_in_trash' => __( 'No Recipes found in Trash', 'redwood-hill' ),
        ),
        'description'  => __( "Available Recipes.", 'redwood-hill' ),
        'supports'     => array('title', 'editor', 'thumbnail'),
        'public'       => true,
        'has_archive'  => 'recipes',
        'hierarchical' => true,
        'rewrite' => array( 'slug' => 'recipes/%recipe%', 'with_front' => false)
    ));
}

add_action( 'init', 'create_product_taxonomy' );

function create_product_taxonomy() {
    register_taxonomy(
        'dairy',
        'products',
        array(
            'label' => 'Dairy Type',
            'hierarchical' => true
        )
    );
}

add_action( 'init', 'create_recipe_taxonomy' );

function create_recipe_taxonomy() {
    register_taxonomy(
        'recipe',
        'recipes',
        array(
            'label' => 'Recipe Category',
            'hierarchical' => true,
            'rewrite' => array('slug' => 'recipes', 'with_front' => false)
        )
    );
}

add_action( 'init', 'create_faq_taxonomy' );

function create_faq_taxonomy() {
    register_taxonomy(
        'faq-category',
        'faqs',
        array(
            'label' => 'FAQ Category',
            'hierarchical' => true,
            'rewrite' => array('slug' => 'faqs', 'with_front' => false)
        )
    );
}


