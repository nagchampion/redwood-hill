<?php
/**
 * The template for displaying all single posts.
 *
 * @package redwood-hill
 */

$post_id = $post->ID;

    $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        
	$title = get_the_title($post_id);
	$author = get_field('exp_author_field', $post_id);
    $content = get_the_content($post_id);
    
   $header = get_field('exp_page_header');
	

	$shortHero = get_field('exp_short_hero');

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<section class="hero short">
             
               <div class="content">
                 <div class="in" style="background-image: url('<?php echo $URL; ?>');"></div>
               </div>
             
            </section><!-- hero -->
			<section class="blog-single__intro">
				<div class="content">
					<h1><?php echo $title; ?></h1>
					<?php if($author): ?>
					<h2 class="author">By <?php echo $author; ?></h2>
					<?php endif; ?>
				</div><!-- content -->
			</section><!-- recipe single intro -->
			<section class="blog-single__content">
				<div class="container">
					<?php the_content($post_id); ?>
					<div class="more-stories-wrapper">
						<a href="<?php echo site_url(); ?>/stories" class="more-stories">More Stories</a>
					</div>
				</div>
			</section>

		<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>