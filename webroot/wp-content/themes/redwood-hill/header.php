<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package redwood-hill
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5PQNNW9');</script>
<!-- End Google Tag Manager -->

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script src="https://use.typekit.net/btv7cuu.js"></script>
<script>try{Typekit.load({ async: false });}catch(e){}</script>
<?php wp_head(); ?>


<meta name="msvalidate.01" content="CE89A1B126DDB2AE5B2830702A093DD1" />

<meta name="google-site-verification" content="ufB4obWAP4RnISgJkAO_2GXncdtSPtucQBLThVUqXHs" />

<meta name="google-site-verification" content="ufB4obWAP4RnISgJkAO_2GXncdtSPtucQBLThVUqXHs" />

<!-- power reviews -->
<meta name="Merchant Group ID" content="79013" />
<meta name="Merchant ID" content="689452" />
<meta name="Site ID" content="1" />
<meta name="Product Look-up URL" content="services.powerreviews.com/ProductLookup.ax?merchantId=689452&pageId=PAGE_ID&variant=VARIANT" />
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5PQNNW9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="hfeed site">
	
	<header id="masthead" class="site-header no-print" role="banner">
		<div class="header-content">
			<div class="site-branding">
			
				<a href="<?php echo esc_url( home_url('/') ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/Redwood_Hill_Farm_Logo.png"></a>
			
			</div>
			<div id="nav-toggle" class="hamburger hamburger--squeeze js-hamburger">
		        <div class="hamburger-box">
		          <div class="hamburger-inner"></div>
		        </div>
	      	</div>
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<a href="<?php echo esc_url( home_url('/') ); ?>/contact" class="upper-nav contact">Contact</a>
				<a href="<?php echo esc_url( home_url('/') ); ?>/store-finder" class="upper-nav find-your-store">
					<span>Store Finder</span>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 18.7 25" enable-background="new 0 0 18.7 25" xml:space="preserve">
					<path fill="#91B56E" d="M9.4,0C4.2,0,0,4.2,0,9.4S9.4,25,9.4,25s9.4-10.5,9.4-15.6S14.5,0,9.4,0z M9.4,12.2c-1.6,0-3-1.3-3-3
						s1.3-3,3-3c1.6,0,3,1.3,3,3S11,12.2,9.4,12.2z"/>
					</svg>

				</a>	
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				<div class="footer-menu-mobile">
					<?php wp_nav_menu( array( 'menu' => 'Footer Menu' ) ); ?>
				</div>
				<div class="search-box">
					<svg id="searchToggle" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
					<path fill="#58514D" d="M2.2,2.2c-2.9,2.9-2.9,7.6,0,10.5c2.5,2.5,6.4,2.8,9.2,1l5.8,5.8c0.6,0.6,1.7,0.6,2.3,0
						c0.6-0.6,0.6-1.7,0-2.3l-5.8-5.8c1.8-2.9,1.5-6.7-1-9.2C9.8-0.7,5.1-0.7,2.2,2.2z M11.4,11.4c-2.2,2.2-5.7,2.2-7.9,0s-2.2-5.7,0-7.9
						c2.2-2.2,5.7-2.2,7.9,0S13.5,9.2,11.4,11.4z"/>
					</svg>
					<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
						
							<input class="search-field" placeholder="Search" value="" name="s" type="search">
						
						<input class="search-submit" value="Go" type="submit">
					</form>
				</div>	
				
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
