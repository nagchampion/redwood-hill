<?php
/**
 * The template for displaying all single posts.
 *
 * @package redwood-hill
 */

//VARS 

	$post_id = $post->ID;

    $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        
    $title = get_the_title($post_id);
    $content = get_the_content($post_id);
    
   	$header = get_field('exp_page_header');
	$subheader = get_field('exp_page_subheader');
	

	$shortHero = get_field('exp_short_hero');

	$exp_recipe_intro = get_field('exp_recipe_intro');
	$exp_recipe_servings = get_field('exp_recipe_servings');

	
	$prepTime = get_field('exp_recipe_prep_time');
	$cookTime = get_field('exp_recipe_cook_time');
	$toteTime = get_field('exp_recipe_total_time');

	$time = time();
	$total = strtotime($toteTime);
	$date = get_the_time('Y-m-d');
	if ($total):
		//echo $total;
	else: 
		//echo 'Bad Time ' . $toteTime;
	endif;
	// $schemaTime = $total - $time;

	function iso8601_duration($seconds)
		{
			$days = floor($seconds / 86400);
			$seconds = $seconds % 86400;

			$hours = floor($seconds / 3600);
			$seconds = $seconds % 3600;

			$minutes = floor($seconds / 60);
			$seconds = $seconds % 60;

			return sprintf('P%dDT%dH%dM%dS', $days, $hours, $minutes, $seconds);
		}

get_header(); ?>
	
	
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); 
			// $author = '';

			// if (get_the_author_meta('display_name')):
			// 	$author = get_the_author_meta('display_name');
			// else:
			// 	$author = "Redwood Hill";
			// endif;
			$author = "Redwood Hill Farm";

		?>
			<?php 
				echo '<script type="application/ld+json">
					{
					"@context": "http://schema.org",
					"@type": "Recipe",
					"author": "' . $author . '",
					"datePublished": "' . $date . '",
					"description": "' . strip_tags($exp_recipe_intro) . '",
					"image": "' . $URL . '",
					"name": "' . $title . '",
					"recipeInstructions": "' . strip_tags(get_the_content($post_id)) . '",
					"recipeYield": "' . $exp_recipe_servings . ' Servings"
					}
				</script>';
			?>
			<section class="no-print hero<?php if($shortHero) echo' short'; ?>">
             
               <div class="content">
                 <div class="in" style="background-image: url('<?php echo $URL; ?>');"></div>
               </div>
             
            </section><!-- hero -->
			<section class="recipe-single__intro">
				<div class="content">
					<h1><?php echo $title; ?></h1>
					<div class="recipe-single__text">
						<?php echo $exp_recipe_intro; ?>
					</div>
					<div class="recipe-single__servings">
						<h6 class="servings"><?php echo $exp_recipe_servings; ?> SERVINGS</h6>
						<div class="recipe-single__time">
							<span>TIME:</span> Preparation: <?php echo $prepTime; ?> <span class="dot">&#183;</span> Cooking: <?php echo $cookTime; ?> <span class="dot">&#183;</span> Total: <?php echo $toteTime; ?>
						</div>
					</div>
					
				</div><!-- content -->
			</section><!-- recipe single intro -->
			<?php $productUsed = get_field('exp_recipe_product_used'); ?>
			<section class="recipe-single__content">
				<div class="<?php if($productUsed): echo 'col col-2-3'; endif; ?>">
				<?php echo get_the_content($post_id); ?>
				</div>
				<?php 
					
        
					if( $productUsed ): ?>

				<div class="col col-1-3 product-used no-print">
					<h3>Product Used:</h3>	
								
						<?php foreach( $productUsed as $p ): 
							
							$post_id = $p->ID;

							$URL = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
								
							$title = get_the_title($post_id);
							$content = $p->post_content;
							$link = get_permalink($post_id);
					
						?>
						<a href="<?php echo $link; ?>" class="no-print">
						<div class="product-used__square">
							
							<div class="in" style="background-image: url('<?php echo $URL; ?>');"></div>
							
						</div>
						</a>
						<h4 class="yes-print"><?php echo $title; ?></h4>
					<?php endforeach; ?>
						
				</div><!-- product used -->

				<?php endif; // if product used exists ?>
				
			</section>
			<section class="recipe-single__content">
			<div class="print-pint no-print">

				<a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $pinterestimage[0]; ?>&description=<?php the_title(); ?>" data-pin-custom="true">
				
					<svg class="pint" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
					<path d="M32.9,0H7.1C3.2,0,0,3.2,0,7.1v25.8C0,36.8,3.2,40,7.1,40h4.5c0-1,0-2,0.1-2.9c0.1-0.7,0.2-1.3,0.3-1.9
						c0.5-2,0.9-3.9,1.4-5.9c0.5-2.1,1-4.2,1.5-6.3c0-0.1,0-0.1,0-0.2c-0.3-0.7-0.5-1.5-0.6-2.3c-0.1-1-0.1-2,0.1-3c0.2-1,0.7-2,1.4-2.8
						c0.7-0.8,1.6-1.2,2.8-1.2c1.4,0,2.4,0.8,2.8,2.1c0.2,0.7,0.2,1.5,0.1,2.2c-0.1,0.8-0.3,1.7-0.6,2.5c-0.4,1.4-0.8,2.8-1.2,4.2
						c-0.2,0.6-0.3,1.1-0.2,1.7c0.1,1.1,0.7,1.9,1.7,2.5c0.9,0.5,1.8,0.6,2.8,0.4c0.8-0.1,1.5-0.5,2.2-1c0.8-0.6,1.4-1.4,2-2.2
						c1-1.7,1.6-3.5,1.9-5.4c0.1-0.7,0.2-1.4,0.2-2.2c0.1-1,0.1-2-0.1-3c-0.3-1.3-0.8-2.6-1.7-3.6c-0.9-1.1-2-1.9-3.3-2.4
						c-1-0.4-2-0.6-3-0.7c-0.9-0.1-1.8-0.1-2.7,0c-0.8,0.1-1.7,0.3-2.5,0.6c-1.3,0.5-2.4,1.1-3.5,2c-0.7,0.6-1.3,1.4-1.9,2.2
						c-1,1.5-1.5,3.2-1.7,5c-0.1,0.8-0.1,1.5,0.1,2.3c0.2,0.9,0.5,1.8,1,2.6c0.2,0.3,0.4,0.5,0.6,0.8c0.1,0.2,0.2,0.4,0.1,0.6
						c0,0.3-0.1,0.5-0.2,0.8c-0.1,0.5-0.2,0.9-0.4,1.4c-0.1,0.3-0.3,0.6-0.7,0.6c-0.2,0-0.3,0-0.5-0.1c-0.5-0.2-1-0.5-1.5-0.9
						c-1.1-0.8-1.8-1.9-2.3-3.2c-0.5-1.4-0.8-2.8-0.8-4.2c0-1,0.1-2,0.3-2.9c0.3-1.4,0.8-2.7,1.6-3.9c0.9-1.5,1.9-2.7,3.3-3.8
						c2.1-1.7,4.4-2.7,7-3.3c1-0.2,2.1-0.3,3.1-0.4c1.2-0.1,2.5,0,3.7,0.2c1.1,0.2,2.2,0.5,3.2,0.9c1.6,0.7,3.1,1.6,4.3,2.8
						c1.3,1.3,2.4,2.8,3,4.5c0.4,1.1,0.7,2.3,0.8,3.5c0.1,0.8,0,1.5,0,2.3c-0.1,1.1-0.2,2.2-0.4,3.2c-0.4,1.7-0.9,3.2-1.7,4.7
						c-0.9,1.6-2,2.9-3.4,4c-0.6,0.5-1.3,0.9-2,1.2c-1,0.4-2,0.7-3.1,0.9c-0.7,0.1-1.4,0.1-2.2,0c-1.1-0.1-2-0.4-2.9-1
						c-0.7-0.4-1.3-0.9-1.7-1.6c0-0.1-0.1-0.1-0.1-0.2c0,0.1,0,0.1,0,0.1c-0.4,1.6-0.8,3.2-1.2,4.8c-0.2,0.8-0.4,1.5-0.7,2.3
						c-0.3,0.9-0.7,1.7-1.2,2.5h18c3.9,0,7.1-3.2,7.1-7.1V7.1C40,3.2,36.8,0,32.9,0z"/>
					</svg>
				</a><!-- pint -->
				<a href="#" onClick="window.print()">

					<svg class="print" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
					<g>
						<rect x="9.9" y="29.5" width="20" height="1.3"/>
						<path d="M26.9,12.2h-4.8V7.5h-9.2v11.8h14V12.2z M25.2,17.8H14.6V16h10.6V17.8z M25.2,15.2H14.6v-1.8h10.6V15.2z"/>
						<path d="M8.9,27.6H31v-6.4H8.9V27.6z M12.7,24.3h2.4v1.8h-2.4V24.3z M9.8,24.4h2.4v1.8H9.8V24.4z"/>
						<path d="M32.9,0H7.1C3.2,0,0,3.2,0,7.1v25.8C0,36.8,3.2,40,7.1,40h25.8c3.9,0,7.1-3.2,7.1-7.1V7.1
							C40,3.2,36.8,0,32.9,0z M32.9,29.5h-1v3.1H8v-3.1H7V19.3l4.1,0V5.6h11.4c0.4,0,0.7,0.1,0.9,0.4l5,4.9c0.2,0.2,0.4,0.5,0.4,0.9v7.4
							h4.1V29.5z"/>
					</g>
					</svg>

				</a>
			</div><!-- end print -->
			<script type="text/javascript">
			(function() {
			    window.PinIt = window.PinIt || { loaded:false };
			    if (window.PinIt.loaded) return;
			    window.PinIt.loaded = true;
			    function async_load(){
			        var s = document.createElement("script");
			        s.type = "text/javascript";
			        s.async = true;
			        s.src = "http://assets.pinterest.com/js/pinit.js";
			        var x = document.getElementsByTagName("script")[0];
			        x.parentNode.insertBefore(s, x);
			    }
			    if (window.attachEvent)
			        window.attachEvent("onload", async_load);
			    else
			        window.addEventListener("load", async_load, false);
			})();
			</script>
			<?php sk_the_page_blocks(); ?>
		<?php endwhile; ?>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>