<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package redwood-hill
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				
				
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'redwood-hill' ); ?></h1>

				
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
