<?php
/**
 * 
 * Template Name: Products
 *
 * @package redwood-hill
 */
$header = get_field('exp_page_header');
$subheader = get_field('exp_page_subheader');
$hero = get_field('exp_hero_image');
$heroURL = $hero['url'];
 
$shortHero = get_field('exp_short_hero');

// Mobile Image Override 
$mobileImage = get_field('exp_mobile_image_override');
$heroMobile = get_field('exp_mobile_hero_image');
$heroURLMobile = $heroMobile['url'];

// Slider Image Override 
$heroSlider = get_field('exp_hero_slider');
$heroSliderImages = get_field('exp_hero_slider_images');
get_header(); ?>


	<div id="primary" class="content-area">
        

        <?php while ( have_posts() ) : the_post(); ?>
           
            <?php if($heroSlider): ?>
            <section class="hero slider<?php if($shortHero) echo ' short'; ?><?php if(!$hero) echo ' no-bg'; ?><?php if($mobileImage) echo ' mobileImage'; ?>">
             
               <div class="sk-slider">
                 <?php if( have_rows('exp_hero_slider_images') ):

                  // loop through the rows of data
                  while ( have_rows('exp_hero_slider_images') ) : the_row();

                      // display a sub field value
                      $slideImage = get_sub_field('image');
                      $slideImageURL = $slideImage['url'];
                  ?>
                  <div class="content sk-slide">
                    <div class="in" style="background-image: url('<?php echo $slideImageURL; ?>');"></div>
                  </div>
                  <?php
                  endwhile;
                endif;
                ?>
      
              </div> <!-- slider -->
              <?php if($mobileImage): ?>
                  <div class="content mobile-content">
                    <div class="mobile-in" style="background-image: url('<?php echo $heroURLMobile; ?>');"></div>
                  </div>
              <?php endif; ?>
               
               <div class="lockup">
                   <h1><?php echo $header; ?></h1>
                   <h2><?php echo $subheader; ?></h2>
                 </div>
            </section><!-- hero image slider -->
            <?php else: ?>
            <section class="hero<?php if($shortHero) echo ' short'; ?><?php if(!$hero) echo ' no-bg'; ?><?php if($mobileImage) echo ' mobileImage'; ?>">
             
               <div class="content">
                 <div class="in" style="background-image: url('<?php echo $heroURL; ?>');"></div>
                 <?php if($mobileImage): ?>
                    <div class="mobile-in" style="background-image: url('<?php echo $heroURLMobile; ?>');"></div>
                 <?php endif; ?>
                 <div class="lockup">
                   <h1><?php echo $header; ?></h1>
                   <h2><?php echo $subheader; ?></h2>
                 </div>
               </div>
            
            </section><!-- hero static -->
            <?php endif; ?>
            <section class="intro-copy">
              <div class="intro-copy__text">
                <?php echo the_content(); ?>
              </div><!-- txt -->
            </section><!-- intro copy -->
            
            

           <?php sk_the_page_blocks(); ?>
        <?php endwhile; // end of the loop. ?>

    </div><!-- #primary -->

<?php get_footer(); ?>
