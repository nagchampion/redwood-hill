


	<div class="recipe-block show-all" data-categories="all<?php foreach ( $terms as $term ) { echo ' '.$term->slug;} ?>">
		<a href="<?php echo $link; ?>">
			<div class="product__image bg-image" style="background-image:url('<?php echo $URL; ?>');"></div>
		</a>
		<a href="<?php echo $link; ?>">
			<div class="block__txt">
				<div class="categories"><?php echo $term->slug; ?></div>
				<div class="product-title"><?php echo $title; ?></div>
				<div class="cooking-time"><?php the_field('exp_recipe_total_time'); ?></div>
			</div>
		</a>
	</div><!-- blog-block -->
