


	<div class="blog-block post-block show-all" data-categories="all<?php foreach ( $terms as $term ) { echo ' '.$term->slug;} ?>">
		<a href="<?php echo $link; ?>">
			<div class="block__image">
				<div class="in" style="background-image: url('<?php echo $URL; ?>');"></div>
			</div>
		</a>
		<a href="<?php echo $link; ?>">
			<div class="block__txt">
				<h3><?php echo $title; ?></h3>
				<?php if($terms): ?>
					<div class="categories"><?php 
						$i = 0;
						$len = count($terms);
						foreach ( $terms as $term ) { 

							if ($i == $len -1):
								echo $term->slug;
							else: 
								echo $term->slug . ', ';
							endif;
						$i++;
						} ?></div>
				<?php endif; ?>
				<div class="txt"><?php echo wp_trim_words( $content, 20, '...' ); ?></div>
			</div>
		</a>
	</div><!-- blog-block -->
