


	<div class="blog-block post-block faq-block" data-categories="all<?php foreach ( $terms as $term ) { echo ' '.$term->slug;} ?>">
		
		<div class="block__txt">
			
			<div class="title-wrapper">
				<h3><?php echo $title; ?></h3>
				<svg version="1.1" class="close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 51.1 51.1" enable-background="new 0 0 51.1 51.1" xml:space="preserve">
				<polygon fill="#57504C" points="48.9,28.5 48.9,22.4 28.9,22.4 28.7,2.2 22.6,2.2 22.6,22.7 2.3,22.4 2.3,28.5 22.6,28.8 22.6,48.8 
					28.7,48.8 28.7,28.8 "/>
				</svg>
			</div>
			<div class="single-faq-content-wrapper">
				<?php echo $content; ?>
			</div><!-- single faq wrapper -->
		</div>
	</div><!-- blog-block -->
