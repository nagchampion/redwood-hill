<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package redwood-hill
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer no-print" role="contentinfo">
		<div class="footer-top">
			<nav>
				<?php wp_nav_menu( array( 'menu' => 'Footer Menu' ) ); ?>
			</nav>
			<div class="footer-goats">
				<img src="<?php echo get_template_directory_uri(); ?>/img/RG_Dairy_Goats.png">
			</div>
			
		</div><!-- footer top -->
		<hr>
		<div class="footer-bottom">
			<div class="footer-newsletter">
				
				<form class="footer-mailchimp" action="https://redwoodhill.us7.list-manage.com/subscribe/post?u=28c15f7b9ea54ba9bf942794c&amp;id=23877d65c0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<h5 class="label-container">SIGN UP FOR OUR NEWSLETTER</h5>
					<input type="email" placeholder="Email Address" name="EMAIL" class="required email" id="mce-EMAIL" />
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_28c15f7b9ea54ba9bf942794c_23877d65c0" tabindex="-1" value=""></div>
					<input type="submit" value="JOIN" name="subscribe" id="mc-embedded-subscribe" class="button"/>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				    
				</form>
			</div><!-- footer newsletter -->
			<div class="footer-social-copy">
				<div class="social-icons">
				<a href="https://www.facebook.com/RedwoodHillFarm/" target="_blank" class="social fb">
					<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="24.45" height="24" viewBox="0 0 24.45 24"><title>fb</title><path d="M24.45,21.4V2.6A2.63,2.63,0,0,0,21.8,0H2.65A2.63,2.63,0,0,0,0,2.6V21.4A2.63,2.63,0,0,0,2.65,24H13.08q0-4.52,0-9c0-.16,0-0.16-0.17-0.16H10c-0.18,0-.18,0-0.18-0.17q0-1.7,0-3.4c0-.17,0-0.17.17-0.17h2.92c0.16,0,.17,0,0.17-0.16,0-.87,0-1.74,0-2.61a6.32,6.32,0,0,1,.14-1.35,4.32,4.32,0,0,1,4.23-3.46c0.46,0,.92,0,1.38,0,0.64,0,1.27.07,1.91,0.1a0.13,0.13,0,0,1,.15.16c0,1,0,2.05,0,3.08,0,0,0,.09,0,0.14H19a5.74,5.74,0,0,0-.73,0,1.23,1.23,0,0,0-1.12.86,2.59,2.59,0,0,0-.11.7c0,0.79,0,1.58,0,2.37,0,0,0,.08,0,0.14h3.7a0.77,0.77,0,0,1,0,.08l-0.13,1c-0.11.83-.22,1.67-0.32,2.5a0.14,0.14,0,0,1-.18.15H17V24H21.8A2.63,2.63,0,0,0,24.45,21.4Z" /></svg>
				</a>
				<a href="https://twitter.com/redwoodhillfarm" target=_"blank" class="social twitter">
					<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26.66 21.67"><title>twitter</title><path d="M26.66 2.56a10.93 10.93 0 0 1-3.14.86 5.49 5.49 0 0 0 2.41-3 11 11 0 0 1-3.47 1.33 5.48 5.48 0 0 0-9.32 5A15.53 15.53 0 0 1 1.86 1a5.48 5.48 0 0 0 1.69 7.3 5.45 5.45 0 0 1-2.48-.68v.07a5.47 5.47 0 0 0 4.39 5.36 5.48 5.48 0 0 1-2.46.1 5.48 5.48 0 0 0 5.11 3.8 11 11 0 0 1-6.81 2.34 11.13 11.13 0 0 1-1.3-.08 15.48 15.48 0 0 0 8.38 2.46A15.46 15.46 0 0 0 23.95 6.1v-.71a11.12 11.12 0 0 0 2.71-2.83z"/></svg>
				</a>
				<a href="https://www.instagram.com/redwoodhillfarm/" target="_blank" class="social insta">
					<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>insta</title><path d="M7.82,7.81a6.38,6.38,0,0,1,8.64,0H24V2.6A2.6,2.6,0,0,0,21.4,0H6.23V6.1h-1V0H4.55V6.1h-1V0H2.86V6.1h-1v-6A2.61,2.61,0,0,0,0,2.6V7.81H7.82ZM17.94,3A1.09,1.09,0,0,1,19,1.94h2.24A1.09,1.09,0,0,1,22.34,3V5.26a1.09,1.09,0,0,1-1.08,1.08H19a1.09,1.09,0,0,1-1.08-1.08V3Z" /><path d="M12.14,7.57a4.94,4.94,0,1,0,4.94,4.94A4.95,4.95,0,0,0,12.14,7.57Zm0,8.39a3.45,3.45,0,1,1,3.45-3.45A3.45,3.45,0,0,1,12.14,16Z" /><path d="M17.31,8.77A6.39,6.39,0,1,1,7,8.77H0V21.4A2.6,2.6,0,0,0,2.6,24H21.4A2.6,2.6,0,0,0,24,21.4V8.77H17.31Z"/></svg>
				</a>
				
			</div>
				<p class="copyright-text">&copy; <?php echo date('Y'); ?> Redwood Hill Farm &amp; Creamery | <a href="<?php echo site_url(); ?>/privacy-policy">Privacy</a></p>
			</div><!-- footer social copy -->
		</div><!-- footer bottom -->

	</footer><!-- #colophon -->
</div><!-- #page -->
<div class="modal">
	<div class="modal-wrapper">
		<div class="modal-close" id="close"></div>
		<input type="zip" placeholder="Enter Zip Code" maxlength="5" id="zipCode">
		<button id="productZip">Find Product</button>
	</div><!-- modal wrapper -->
</div><!-- modal -->
<?php wp_footer(); ?>

</body>
</html>
