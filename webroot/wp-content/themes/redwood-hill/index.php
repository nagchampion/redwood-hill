<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package redwood-hill
 */

$exclude = get_cat_ID('Press Release');
get_header(); ?>


	<div id="primary" class="content-area">
        

        
           
           
            
            <section class="blog-categories">
              
                <div class="filters filters-recipe">
                  <!--<div class="lbl"><?php the_field('sort_by','option');?> <img class="arrow" src="<?php echo get_template_directory_uri(); ?>/img/arrows/arrow-down.svg" /></div>-->
                  <span class="filter-lead">Stories:</span> 
                  <ul>
                    <li class="filter-item selected"><a href="<?php echo site_url(); ?>/stories">All</a></li>
                    <?php 
                     $terms = get_terms(
                      array(
                          'taxonomy' => 'category',
                          'hide_empty' => false,
                          'post__not_in' => 'uncategorized',
                          'exclude' => array( $exclude )
                      )
                     );
                     if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                         foreach ( $terms as $term ) {
                          $link = get_term_link( $term );
                          
                        echo '<li class="filter-item"><a href="'. get_category_link( $term->term_id ) . '">' . $term->name . '</a></li>';     
                         }
                                    
                     }
                     ?>
                   </ul>    
                </div>
              
            </section><!-- recipe categories -->
            <section class="blog-content">
              <div class="the-blog-wrapper">
              <header class="blog-header">
              <h1 class="page-title">
                <?php
                  if ( is_category() ) :
                    single_cat_title();

                  elseif ( is_tag() ) :
                    single_tag_title();

                  elseif ( is_author() ) :
                    printf( __( 'Author: %s', 'redwood-hill' ), '<span class="vcard">' . get_the_author() . '</span>' );

                  elseif ( is_day() ) :
                    printf( __( 'Day: %s', 'redwood-hill' ), '<span>' . get_the_date() . '</span>' );

                  elseif ( is_month() ) :
                    printf( __( 'Month: %s', 'redwood-hill' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'redwood-hill' ) ) . '</span>' );

                  elseif ( is_year() ) :
                    printf( __( 'Year: %s', 'redwood-hill' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'redwood-hill' ) ) . '</span>' );

                  elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
                    _e( 'Asides', 'redwood-hill' );

                  elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
                    _e( 'Galleries', 'redwood-hill' );

                  elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
                    _e( 'Images', 'redwood-hill' );

                  elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
                    _e( 'Videos', 'redwood-hill' );

                  elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
                    _e( 'Quotes', 'redwood-hill' );

                  elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
                    _e( 'Links', 'redwood-hill' );

                  elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
                    _e( 'Statuses', 'redwood-hill' );

                  elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
                    _e( 'Audios', 'redwood-hill' );

                  elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
                    _e( 'Chats', 'redwood-hill' );

                  else :
                    _e( 'All Stories', 'redwood-hill' );

                  endif;
                ?> 
              </h1>
              <?php
                // Show an optional term description.
                $term_description = term_description();
                if ( ! empty( $term_description ) ) :
                  printf( '<div class="taxonomy-description">%s</div>', $term_description );
                endif;
              ?>
            </header><!-- .page-header -->
              <?php 

                      

                      $postQuery = array(
                        'posts_per_page' => -1,
                        'orderby'        => 'menu_order',
                        'post_type'      => 'post',
                        'tax_query' => array(
                          'relation' => 'AND',
                          array(
                              'taxonomy' => 'category',
                              'field'    => 'term_id',
                              'terms'    => array( $exclude ),
                              'operator' => 'NOT IN',
                          ),
                      )
                    );

                  query_posts( $postQuery ); ?>

                  
                  
                  <?php if(get_posts($postQuery)) : 

                      // The Loop
                      while ( have_posts() ) : the_post();
                      
                      
                        global $post;
                        $post_id = $post->ID;

                        $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            
                        $title = get_the_title($post_id);
                        $content = get_the_content($post_id);
                        $link = get_permalink($post_id);
                        $terms = get_the_terms($post_id, 'category');
                        

                         include(locate_template('inc/post-block.php')); ?>
                        
                      
                      <?php endwhile;
                            endif; 
                      // Reset Query
                      wp_reset_query();
              ?>
              </div><!-- the recipes wrapper -->
            </section><!-- receipe block -->
        

    </div><!-- #primary -->

<?php get_footer(); ?>
