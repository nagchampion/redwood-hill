<?php
/**
 * 
 * Template Name: FAQs
 *
 * @package redwood-hill
 */
$header = get_field('exp_page_header');
$subheader = get_field('exp_page_subheader');
$hero = get_field('exp_hero_image');
$heroURL = $hero['url'];
 
$shortHero = get_field('exp_short_hero');
get_header(); ?>


	<div id="primary" class="content-area">
        
      <section class="faqs-section">
        <?php while ( have_posts() ) : the_post(); ?>
           
            <!--<section class="blog-categories">
              
                <div class="filters filters-recipe">
                  
                  <span class="filter-lead">FAQs:</span> 
                  <ul>
                    <li class="filter-item selected"><a data-type="team-category" data-filter="all">All</a></li>
                    <?php 
                     $terms = get_terms(
                      array(
                          'taxonomy' => 'faq-category',
                          'hide_empty' => false
                      )
                     );
                     if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                         foreach ( $terms as $term ) {
                          $link = get_term_link( $term );
                   
                        echo '<li class="filter-item faq-filter"><a href="#" data-filter="'.$term->slug.'">' . $term->name . '</a></li>';     
                         }
                                    
                     }
                     ?>
                   </ul>    
                </div>
              
            </section>-->
            <?php 

              $terms = get_terms('faq-category');
                
                foreach ($terms as $term) {
                  $queryTerm = strtolower($term->name);

                  
            ?>
            
              <div class="the-faqs-wrapper">
                <h2 class="faq-header">FAQ: <?php echo $term->name; ?></h2>
              <?php 

                      $faqQuery = array(
                        'posts_per_page' => -1,
                        'orderby'        => 'menu_order',
                        'post_type'      => 'faqs',
                        'tax_query' => array(
                        array (
                            'taxonomy' => 'faq-category',
                            'field' => 'slug',
                            'terms' => $queryTerm,
                        )
                      ),
                    );

                  query_posts( $faqQuery );
                  query_posts( $faqQuery ); ?>

                  
                  
                  <?php if(get_posts($faqQuery)) : 

                      // The Loop
                      while ( have_posts() ) : the_post();
                      
                      
                        global $post;
                        $post_id = $post->ID;

                        $URL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            
                        $title = get_the_title($post_id);
                        $content = get_the_content($post_id);
                        $link = get_permalink($post_id);
                        $terms = get_the_terms($post_id, 'faq-category');
                        

                         include(locate_template('inc/faq-block.php')); ?>
                        
                      
                      <?php endwhile;
                            endif; 
                      // Reset Query
                      wp_reset_query();
              ?>
              </div><!-- the faqs wrapper -->
            
            <?php } // end foreach ?>
          </section><!-- faq section block -->
            <?php sk_the_page_blocks(); ?>

        <?php endwhile; // end of the loop. ?>

    </div><!-- #primary -->

<?php get_footer(); ?>
