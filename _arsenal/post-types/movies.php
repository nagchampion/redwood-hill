
    //
    // Movies
    //
    register_post_type('movies', array(
        'labels'       => array(
            'name'               => __( 'Movies', 'sherman' ),
            'singular_name'      => __( 'Movie', 'sherman' ),
            'add_new_item'       => __( 'Add new Movie', 'sherman' ),
            'edit_item'          => __( 'Edit Movie', 'sherman' ),
            'new_item'           => __( 'New Movie', 'sherman' ),
            'view_item'          => __( 'View Movie', 'sherman' ),
            'search_items'       => __( 'Search Movies', 'sherman' ),
            'not_found'          => __( 'No Movies found', 'sherman' ),
            'not_found_in_trash' => __( 'No Movies found in Trash', 'sherman' ),
        ),
        'description'  => __( "Available movies.", 'sherman' ),
        'supports'     => array('title', 'editor'),
        'public'       => true,
        'hierarchical' => false
    ));
